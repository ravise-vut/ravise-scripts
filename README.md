## Synopsis

Various scripts/files I wrote primarily for myself, but can be useful for other people too.

## Installation

Generally, clone the repository, `cd` into project dir and `make install` (makefiles not supplied yet, do it by hand). See every file's docs.

## License

WTFPL 2.0 it is...

---

## Files/subprojects

### conkyrc

#### Installation

Copy to your `~/.conkyrc`. Add `conky` to your startup routines.

#### Screenshot

![conkyrc](/imgs/conkyrc.png?raw=true)

---

### bcrc

Autoload the bc math library, do some basic setup and function aliases. Heavily inspired by [creack](https://github.com/creack/dotfiles/blob/master/.bcrc).

#### Installation

Copy to your `~/.bcrc`. Add `export BC_ENV_ARGS="-l $HOME/.bcrc"` to your `~/.bashrc`.


---
