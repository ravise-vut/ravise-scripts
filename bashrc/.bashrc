# ~/.bashrc: executed by bash(1) for non-login shells.
# see /usr/share/doc/bash/examples/startup-files (in the package bash-doc)
# for examples

# If not running interactively, don't do anything
case $- in
    *i*) ;;
      *) return;;
esac

if [ -d "$HOME/.bashrc.d/" ]; then
	for file in "$HOME"/.bashrc.d/*.bashrc; do
		source "$file"
	done
fi

if [ -d "$HOME/.bash_completion.d/" ]; then
	for file in "$HOME"/.bash_completion.d/*.completion; do
		source "$file"
	done
fi
