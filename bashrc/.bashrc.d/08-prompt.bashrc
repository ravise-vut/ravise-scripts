# set variable identifying the chroot you work in (used in the prompt below)
if [ -z "${debian_chroot:-}" ] && [ -r /etc/debian_chroot ]; then
    debian_chroot=$(cat /etc/debian_chroot)
    debian_chroot=":${debian_chroot}"
fi
BOLD="\[$(tput bold)\]";
RESET="\[$(tput sgr0)\]";

# Colours
FG_BLUE="\[$(tput setf 1)\]";
FG_GREEN="\[$(tput setf 2)\]";
FG_LBLUE="\[$(tput setf 3)\]";
FG_RED="\[$(tput setf 4)\]";
FG_MAG="\[$(tput setf 5)\]";
FG_YELLOW="\[$(tput setf 6)\]";
FG_WHITE="\[$(tput setf 7);"

# Escape sequences
TIME="\A";
USR="\u";
HOST="\H";
WDIR="[\w]";
EXITSTAT="[\$?:\j]";
DOLLAR="\\$";

export PROMPT_COMMAND="history -a; history -c; history -r; "
export PS1="${BOLD}${EXITSTAT}${RESET} ${TIME} ${FG_YELLOW}${USER}${RESET}@${FG_LBLUE}${HOST}${RESET}:${debian_chroot}${FG_GREEN}${WDIR}${RESET}${FG_MAG}\$(__git_ps1)${RESET}${DOLLAR} "
export TASK="${TASK:-bash}"

case $TERM in
	xterm*)
		export PROMPT_COMMAND="${PROMPT_COMMAND}echo -ne \"\033]0;\${TASK} | \${USER}@\${HOSTNAME}: \${PWD/\$HOME/\~}\007\""
		;;
esac

bind '"\e[A": history-search-backward'
bind '"\e[B": history-search-forward'

export BC_ENV_ARGS="-l $HOME/.bcrc"
