export GOROOT=$HOME/opt/go-dist
export GOPATH=$HOME/go/workspace
export GOBIN=$HOME/go/bin

export PATH=$PATH:$GOROOT/bin:$GOBIN
