# bcrc file loaded when bc is launched
export BC_ENV_ARGS="-l $HOME/.bcrc"

# GCC_COLOURS
export GCC_COLORS='error=01;31:warning=01;35:note=01;36:caret=01;32:locus=01:quote=01'

# Lookup in history based on current line
bind '"\e[A": history-search-backward'
bind '"\e[B": history-search-forward'
