# bashrc files

Modified the standard `~/.bashrc` to split it into submodules and enable global user-defined place to store bash completion files.

Most of `*.bashrc` files should be simple enough to be perfectly readable. `01-misc.bashrc` specifies a TRAP which ensures `~/.bash_logout` is executed when non-login shell is ended too.

## Installation

Copy `.bashrc.d/*` to `~/.bashrc.d/*`. Copy, `.bashrc` to `~/.bashrc` or add

```
if [ -d "$HOME/.bashrc.d/" ]; then
	for file in "$HOME"/.bashrc.d/*.bashrc; do
		source "$file"
	done
fi

if [ -d "$HOME/.bash_completion.d/" ]; then
	for file in "$HOME"/.bash_completion.d/*.completion; do
		source "$file"
	done
fi
```

into your `~/.bashrc`


## Screenshot
### Bash prompt
![bashrc prompt](/imgs/bashrc-prompt.png)

[exit-status-of-last-command:background-jobs] time-in-hh:mm-format user@host:chroot[pwd] (git-branch)$
