## nano syntaxes

Few syntaxes for nano editor. Formats maily not found elsewhere.

Contents:

* todotxt (patterns: *todo.txt, *done.txt)

### Installation

Copy, `.nano/*.rc` to `~/.nano/*.rc`. Copy `.nanorc` to `~/.nanorc` or otherwise ensure that `~/.nano/*.rc` is sourced by `nano`.

### Screenshot

![nanorc - todotxt](/imgs/nanorc-todotxt.png?raw=true)

