## mpd.sh

Commandline "client" to mpd written in bash. Depends only on mpc. Written for interaction with `fluxbox` hotkeys and `conky`.

### Installation

Add `mpd.sh` somewhere into your `$PATH`. If you want bash completion, `source` `mpdsh.completion` into your environment.

### Help
```
mpd.sh next
        moves playlist forward
mpd.sh prev
        moves playlist back
mpd.sh toggle
        toggles playing
mpd.sh playing
        outputs name of file being played
mpd.sh queue
        shows current queue status (playing/total)
mpd.sh vol up
        moves volume 1 step up
mpd.sh vol down
        moves volume 1 step down
mpd.sh vol get
        outputs current volume
mpd.sh vol display
        displays current volume
mpd.sh local|http [on|off]
        sets OUTPUT on or off
        if no $2 is present, sets OUTPUT on while disabling the other
mpd.sh both
        sets http and local outputs ON
Set your variables to suit your needs
 - in your workspace or
 - in /home/you/.mpdshrc.
Current settings:
        mpd.sh resides in '/home/you/bin/mpd.sh'
        $mpdhost          'localhost'
        $mpdpass          'adminpassword'
        $mpdport          '6600'
        $httpID           '1'
        $localID          '2'
        $RCFILE           'not present'
```

---

## todotxt

Simple script to help me work with [todo.txt](http://todotxt.org/) file format.

### Installation

Add `todotxt` somewhere into your `$PATH`. If you want bash completion, `source` `todotxt.completion` into your environment.

### Help
```
todotxt add
        adds new task to your $TODOFILE and opens $EDITOR on $TODOFILE
todotxt cat todo|done|both
        displays your $FILE
todotxt clean todo|done|both
        cleans your $FILE of empty lines
todotxt done
        moves completed items from $TODOFILE into $DONEFILE
todotxt edit todo|done
        calls your $EDITOR on $FILE
todotxt sort todo|done|both
        performs alphabetical sort on your $FILE

Set your $TODOFILE and $DONEFILE to suit your needs
 - in your workspace or
 - in /home/you/.todotxtrc.
Current settings:
        todotxt resides in '/home/you/bin/todotxt'
        $RCFILE            'present'
        $TODOFILE          '/home/you/sync/todo.txt'
        $DONEFILE          '/home/you/sync/done.txt'
        $EDITOR            'nano'
```
