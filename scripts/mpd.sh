#!/bin/bash
THIS=$(basename $0)
RCFILE="$HOME/.mpdshrc";
RCEXISTS="not present"

if [ -f "$RCFILE" ]; then
	source "$RCFILE"
	RCEXISTS="present"
fi

mpdhost=${mpdhost:-"localhost"}
mpdpass=${mpdpass:-"adminpassword"}
mpdport=${mpdport:-"6600"}
httpID=${httpID:-"1"};
localID=${localID:-"2"};

MPDPARAMS="-h $mpdpass@$mpdhost -p $mpdport"

USAGE="$THIS next
	moves playlist forward
$THIS prev
	moves playlist back
$THIS toggle
	toggles playing
$THIS playing
	outputs name of file being played
$THIS queue
	shows current queue status (playing/total)
$THIS vol up
	moves volume 1 step up
$THIS vol down
	moves volume 1 step down
$THIS vol get
	outputs current volume
$THIS vol display
	displays current volume
$THIS local|http [on|off]
	sets OUTPUT on or off
	if no \$2 is present, sets OUTPUT on while disabling the other
$THIS both
	sets http and local outputs ON
Set your variables to suit your needs
 - in your workspace or
 - in $RCFILE.
Current settings:
	$THIS resides in '$0'
	\$mpdhost          '$mpdhost'
	\$mpdpass          '$mpdpass'
	\$mpdport          '$mpdport'
	\$httpID           '$httpID'
	\$localID          '$localID'
	\$RCFILE           '$RCEXISTS'"

### FUN begins here ###
function mpdsh_vol_ctrl() {
	step=5
	case $1 in
		up) mpc $MPDPARAMS volume +${step};;
		down) mpc $MPDPARAMS volume -${step};;
		*) mpc $MPDPARAMS volume $1;;
	esac
	mpdsh_vol_display;
}

function mpdsh_vol_display() {
	osdctl -l "MPD volume",$(mpdsh_vol_get);
}

function mpdsh_vol_get() {
	mpc $MPDPARAMS | grep volume | grep -o '[[:digit:]]\+';
}

function mpdsh_next() {
	mpc $MPDPARAMS next;
}

function mpdsh_prev() {
	mpc $MPDPARAMS prev
}

function mpdsh_toggle() {
	mpc $MPDPARAMS toggle
}

function mpdsh_playing() {
	mpc $MPDPARAMS -f %file% | head -n 1
}

function mpdsh_queue() {
	lines=$(mpc $MPDPARAMS | sed '/#/!d; s/#//g' | awk '{print $2}')
	if [ -z "$lines" ]; then
		echo "n/a"
	else
		echo "$lines"
	fi
}

function mpdsh_control_stream() { # $1=command:enable|disable[ only]; $2=streamID:1-...
	mpc $MPDPARAMS $1 $2
}
### FUN endsh here ###

### MAIN begins here ###
case $1 in
	vol|volume)
		case $2 in
			up) mpdsh_vol_ctrl up;;
			down) mpdsh_vol_ctrl down;;
			get) mpdsh_vol_get;;
			display) mpdsh_vol_display;;
			*)
		esac;;
	next) mpdsh_next;;
	prev) mpdsh_prev;;
	toggle) mpdsh_toggle;;
	playing) mpdsh_playing;;
	queue) mpdsh_queue;;
	http)
		stream="$httpID";
		case $2 in
			on) command="enable";;
			off) command="disable";;
			*) command="enable only";;
		esac;
		mpdsh_control_stream "$command" "$stream"
		;;
	local)
		stream="$localID";
		case $2 in
			on) command="enable";;
			off) command="disable";;
			*) command="enable only";;
		esac;
		mpdsh_control_stream "$command" "$stream"
		;;

	both) mpdsh_control_stream "enable" "${localID}"; mpdsh_control_stream "enable" "${httpID}";;
	help|--help|-h)
		echo "$USAGE"
	;;
	*) exit 2
esac;
### MAIN ends here ###

